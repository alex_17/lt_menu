(function ($) {

    $.fn.lt_menu = function (option){
        var elm = $(this);

        option.o = {
            title: option.title ? option.title : 'Каталог',
            width_button: option.width_button ? option.width_button : 120,
            padding_block: option.padding_block ? option.padding_block : 10,
            level: option.level <= 5 ? (option.level ? option.level : 5) : 5,
            src: option.src ? option.src : 'catalog.json',
            data: option.data ? option.data : {},
            width_level: option.width_level ? option.width_level : []
        };

        option.init = function () {
            elm.append('<div class="button"><i></i><span>'+option.o.title+'</span></div>');
            elm.find('.button').css({
                'width': option.o.width_button
            });
            elm.append('<div class="list" data-level-group="1" style="display: none;"></div>');
        };

        option.init();

        option.meta = {
            button: {
                elem: this.find('.button'),
                outer_width: this.find('.button').outerWidth(),
                outer_height: this.find('.button').outerHeight(),
                position: this.find('.button').offset()
            },
            list: {
                elem: this.find('.list'),
                clickButton: function () {
                    var level_1 = elm.find('[data-level-group="1"]');
                    $.each( option.meta.json(), function (i, v) {
                        option.meta.template.item(level_1, 1, i, v);
                        if(v.result.length != 0 && 1 < option.o.level){
                            var level_2 = option.meta.template.list(level_1, 1, 2, i);
                            $.each( v.result, function (i, v) {
                                option.meta.template.item(level_2, 2, i, v);
                                if(v.result.length != 0 && 2 < option.o.level){
                                    var level_3 = option.meta.template.list(level_2, 2, 3, i);
                                    $.each( v.result, function (i, v) {
                                        option.meta.template.item(level_3, 3, i, v);
                                        if(v.result.length != 0 && 3 < option.o.level){
                                            var level_4 = option.meta.template.list(level_3, 3, 4, i);
                                            $.each( v.result, function (i, v) {
                                                option.meta.template.item(level_4, 4, i, v);
                                                if(v.result.length != 0 && 4 < option.o.level){
                                                    var level_5 = option.meta.template.list(level_4, 4, 5, i);
                                                    $.each( v.result, function (i, v) {
                                                        option.meta.template.item(level_5, 5, i, v);
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    });
                },
                outerHeightInt: function () {
                    var div = elm.find('.list [data-level]');
                    $.each(div, function (i, v) {
                        $(v).attr('data-width', $(v).outerHeight());
                    })
                },
                dinamicParentsHeight: function (parents) {
                    var result = [];
                    parents.each(function (i1, v1) {
                        var i = 0;
                        $(v1).find('> [data-level]').each(function (i2, v2) {
                            i += Number($(v2).attr('data-width'));
                        });
                        result[i1] = i;
                    });
                    return Math.max.apply(null, result);
                },
                filterWidthButton: function (level) {
                    if(option.o.width_level.length != 0 && typeof option.o.width_level[level] !== "undefined"){
                        return option.o.width_level[level];
                    } else {
                        return option.o.width_button;
                    }
                }
            },
            template: {
                list: function (e, l, g, i) {
                    var width_button = option.meta.list.filterWidthButton(l);
                    var left = 'left: '+(width_button + (option.o.padding_block * 2))+'px;';
                    if(l == 1){
                        left = 'left: '+(width_button + (option.o.padding_block * 2) - 6)+'px;';
                    }
                    e.find('[data-level="'+l+'"][data-id='+i+']').append('<div data-level-group="'+g+'" style="'+left+'"></div>');
                    return e.find('[data-level="'+l+'"][data-id='+i+'] [data-level-group='+g+']');
                },
                item: function (e, l, i, v) {
                    var width_button = option.meta.list.filterWidthButton(l);
                    var i_tag = '';
                    var padding_block = 'padding: '+(option.o.padding_block)+'px;';
                    var style_width = 'width: '+width_button+'px;';
                    if(l == 1){
                        style_width = 'width: '+(width_button - 6)+'px;';
                    }
                    if(v.result.length != 0 && l < option.o.level){
                        i_tag = '<i></i>'
                    }
                    e.append('<div data-level="'+l+'" data-id="'+i+'"></div>');
                    e.find('[data-level="'+l+'"][data-id='+i+']').append('<a href="'+v.link+'" style="'+style_width+padding_block+'"><span>'+v.title+'</span>'+i_tag+'</a>');
                }
            },
            json: function () {
                return $.ajax({
                    type: 'post',
                    url: option.o.src,
                    async: false,
                    dataType: 'json',
                    data: option.o.data,
                    statusCode:{
                        404:function(){
                            console.log('Ввидите правельный адрес json массива!')
                        }
                    },
                    success: function (data) {
                        return data;
                    }
                }).responseJSON;
            }
        };

        option.meta.button.elem.on('click', function () {
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                option.meta.list.elem.slideUp(300, function () {
                    option.meta.list.elem.html("");
                });
            } else {
                $(this).addClass('active');
                option.meta.list.clickButton();
                option.meta.list.elem.slideDown(300, function () {
                    option.meta.list.outerHeightInt();
                    var parents, height;
                    elm.find('[data-level]').hover(function () {
                        option.meta.list.outerHeightInt();
                        var s = $(this).find('> [data-level-group]');
                        if(s.length != 0){
                            parents = $(s.find('[data-level]:eq(1)')).parents('[data-level-group]');
                            height = option.meta.list.dinamicParentsHeight(parents);
                            elm.find('.list').height(height);
                        } else {
                            parents = $(this).parents('[data-level-group]');
                            height = option.meta.list.dinamicParentsHeight(parents);
                            elm.find('.list').height(height);
                        }
                    }, function () {
                        parents = $(this).parents('[data-level-group]');
                        height = option.meta.list.dinamicParentsHeight(parents);
                        elm.find('.list').height(height);
                    });
                });
            }
        });

        elm.parents(document).on('click', function (e) {
            if (elm.has(e.target).length === 0){
                option.meta.button.elem.removeClass('active');
                option.meta.list.elem.slideUp(300, function () {
                    option.meta.list.elem.html("");
                });
            }
        });
    };

})(jQuery);