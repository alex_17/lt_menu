# JSON меню

## Подключения

    <link rel="stylesheet" href="/css/lt_menu.min.css">

    <script src="/js/jquery.js"></script>

    <script src="/js/lt_menu.min.js"></script>

## Инициализация

    $('.category').lt_menu();

или с параметрами

    $('.category').lt_menu({

       title: 'Продукция',

       src: 'data/catalog.php',

       level: 5,

    });

### Опции

#### Заголовок меню

    Параметр: title

    Значения: строка (default 'Каталог')

#### Адрес JSON файла

    Параметр: src

    Значения: строка (default 'catalog.json')

#### DATA параметры передоваемые на JSON файла

    Параметр: data

    Значения: массив (default {})

#### Максимальный уровень вложенности в меню

    Параметр: level

    Значения: число (default 5)

    Максимум уровней: 5

#### Ширина кнопок меню

    Параметр: width_button

    Значения: число (default 120)

#### Ширина кнопок меню уровню

    Параметр: width_level

    Значения: массив (default [])

    Пример: [210, 220, 240, 300, 260]

    Описание: Не указывая ширину по уровню меню, будет приминятся ширина из опции width_button

#### Внутренние оступы кнопок меню

    Параметр: padding_block

    Значения: число (default 10)

## Скриншоты

![](http://ltrim.ru/bitbucket/ltmenu.png)

## Пример JSON массива

    [
      {
        "title":"Пункт первый",
        "link": "/",
        "result" : []
      },
      {
        "title":"Пункт второй",
        "link": "/",
        "result" : [
          {
            "title":"Пункт первый",
            "link": "/",
            "result" : []
          }
        ]
      },
      {
        "title":"Пункт третий",
        "link": "/",
        "result" : []
      }
    ]


