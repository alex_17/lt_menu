<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Пользователь</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/lt_menu.min.css">
</head>
<body>

<div class="lt_menu"></div>

<script src="js/jquery.js"></script>
<script src="js/lt_menu.min.js"></script>
<script>
    $(document).ready(function () {
        $('.lt_menu').lt_menu({
            title: 'Продукция',
            src: 'data/catalog.json',
            level: 5,
            width_button: 150,
            border_menu: 4,
            border_block: 1,
            width_level: [210, 220, 240, 300, 260]
        });
    })
</script>
</body>
</html>